from cffi import FFI  # better alternative to ctypes
ffi = FFI()
ffi.cdef("""
    int double(int);
""")
C = ffi.dlopen("/Users/vitalykravchenko/Documents/Rust/python_rust/target/release/libpython.dylib")

print(C.double(3))  # 6
