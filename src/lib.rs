// To test this execute:
// cargo test

// To build:
// cargo build
// cargo build --release

// Use 'cargo new project_name' if this file isn't part of a project.
// This file should be inside the 'src' directory of the project, e.g.:
// src/lib.rs

// Cargo.toml should contain a section like this:
// [lib]
// name = "mylib"
// crate_type = ["dylib"]

#[no_mangle]
pub extern fn double(x: i32) -> i32 {
    x * 2
}

#[test]
fn test_double() {
    assert!(double(3) == 6)
}
